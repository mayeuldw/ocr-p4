
function onUpdateNumber()
{
    var visitors = parseInt($("#" + $("#table").attr("data-form-number")).val());
    $(".openedDay").each(function(){
        if(parseInt($(this).attr("data-q")) < visitors)
        {
            $(this).css("background-color", "darkred");
        }
        else
        {
            $(this).css("background-color", "inherit");
        }
    });
}

function onDateClick(el)
{
    var count = el.attr("data-q");
    if(parseInt(count) < 0)
    {
        return false;
    }
    if(parseInt(count) > parseInt($("#" + $("#table").attr("data-form-number")).val()))
    {
        $(".openedDaySpan").css("border", "0px");
        el.find(".openedDaySpan").css("border", "solid darkgreen 3px");

        $("#" + $("#table").attr("data-form-date")).val(el.attr("data-date"));
        $(".submitBtn").prop("disabled", false);
    }
}
$("form input:not([type=\"submit\"])").on("keydown", function(e) {if (e.keyCode === 13){ e.preventDefault();}});
