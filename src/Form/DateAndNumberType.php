<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 15/03/2018
 * Time: 15:06
 */

namespace App\Form;

use App\Entity\Order;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateAndNumberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', IntegerType::class)
            ->add('date', DateType::class, ['required'=>false, 'widget'=>'single_text'])
            ->add('send', SubmitType::class)
            ->add('previous', SubmitType::class)
            ->add('next', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => Order::class,
            'validation_groups'=>['dateAndNumber']
            ]
        );
    }
}

