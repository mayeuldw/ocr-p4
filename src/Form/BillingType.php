<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 09/04/2018
 * Time: 09:25
 */

namespace App\Form;


use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('email2', EmailType::class)
            ->add('name', TextType::class)
            ->add('firstName', TextType::class)
            ->add('formAddress1', TextType::class)
            ->add('formAddress2', TextType::class, ['required'=>false])
            ->add('formCP', TextType::class)
            ->add('formCity', TextType::class)
            ->add('formCountry', CountryType::class)
            ->add('send', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => Order::class,
            'validation_groups'=>['billing']
            ]
        );
    }
}
