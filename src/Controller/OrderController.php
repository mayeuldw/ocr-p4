<?php
namespace App\Controller;

use App\Entity\ShippingInformation;
use App\Form\BillingType;
use App\Form\VisitorType;
use App\Manager\Calendar;
use App\Form\DateAndNumberType;

use App\Manager\OrderManager;
use App\Manager\PriceManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;



class OrderController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request      $request
     * @param Calendar     $calendar
     * @param OrderManager $orderManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidOrderException
     */
    public function dateAndNumber(Request $request, Calendar $calendar, OrderManager $orderManager)
    {
        $order = $orderManager->getOrder(OrderManager::DATE_AND_NUMBER);
        $form = $this->createForm(DateAndNumberType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $order = $form->getData();
            if ($form->get('previous')->isClicked()) {
                $calendar->previousMonth();
            } else if ($form->get('next')->isClicked()) {
                $calendar->nextMonth();
            } else {
                if ($form->isValid()) {
                    $order->initializeTickets();
                    return $this->redirectToRoute('visitorInformation');
                }
            }
        }

        return $this->render(
            'date_and_number.twig', array(
            'form'          => $form->createView(),
            'calendar'      => $calendar
            )
        );
    }

    /**
     * @Route("/visitor", name="visitorInformation")
     * @param Request      $request
     * @param OrderManager $orderManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidOrderException
     */
    public function visitorInformation(Request $request, OrderManager $orderManager)
    {
        $order = $orderManager->getOrder(OrderManager::VISITOR);
        $form = $this->createForm(VisitorType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('billingInformation');
        }

        return $this->render('visitor_information.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/billing", name="billingInformation")
     * @param Request      $request
     * @param OrderManager $orderManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidOrderException
     */
    public function billingInformation(Request $request, OrderManager $orderManager)
    {
        $order = $orderManager->getOrder(OrderManager::BILLING);
        $form = $this->createForm(BillingType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('ticketTypeAndStripe');
        }

        return $this->render('billing_information.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/summary", name="ticketTypeAndStripe")
     * @param Request      $request
     * @param OrderManager $orderManager
     * @param PriceManager $priceManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidOrderException
     */
    public function ticketTypeAndStripe(Request $request, OrderManager $orderManager, PriceManager $priceManager)
    {
        $order = $orderManager->getOrder(OrderManager::SUM_UP);
        $error = [];
        if ($request->query->get('token', null) !== null) {
            if ($orderManager->processStripePayment(
                $order,
                $request->query->get('token'),
                $request->request->get('stripeToken'),
                $request->query->get('price')
            )
            ) {
                return $this->redirectToRoute('success');
            } else {
                $error[] = 'Une erreur c\'est produite, le payement n\'a pas été effectué';
            }

        }
        return $this->render(
            'ticket_type_and_stripe.twig', [
            'daySettings' => $orderManager->getAvailableDaySettings($order),
            'orderTypes' => $orderManager->processTicketType($order),
            'order' => $order,
            'priceManager' => $priceManager,
            'error' => $error
            ]
        );
    }

    /**
     * @Route("/success", name="success")
     * @param OrderManager $orderManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidOrderException
     */
    public function success(OrderManager $orderManager)
    {
        $order = $orderManager->getPaidOrder();
        return $this->render('success.twig', ['order' => $order]);
    }
}
