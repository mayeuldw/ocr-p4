<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 23/03/2018
 * Time: 09:31
 */

namespace App\EventListener;

use App\Exception\InvalidOrderException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\RouterInterface;

class ExceptionListener
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }


    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof InvalidOrderException) {
            $response = new RedirectResponse($this->router->generate('homepage'));
            $event->setResponse($response);
        }
    }
}