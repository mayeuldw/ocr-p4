<?php
    /**
     * Created by PhpStorm.
     * User: Mayeul
     * Date: 15/03/2018
     * Time: 13:21
     */

namespace App\Validator\Constraints;

    use Symfony\Component\Validator\Constraint;

    /**
     * @Annotation
     */
class OneAdultTicket extends Constraint
{
    public $message = 'Votre commande doit comporter au moins un billet pour un adulte.';

}
