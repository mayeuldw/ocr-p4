<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 15/03/2018
 * Time: 13:21
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotFull extends Constraint
{
    public $message = 'Le nombre de billet demandé n\'est pas disponible pour la date souhaité';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}