<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 21/05/2018
 * Time: 08:36
 */

namespace App\Validator\Constraints;

use App\Manager\BankHolidaysManager;

use App\Manager\OrderManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OneAdultTicketValidator extends ConstraintValidator
{
    /**
     * @var OrderManager
     */
    private $orderManager;

    public function __construct(OrderManager $orderManager)
    {
        $this->orderManager = $orderManager;
    }

    public function validate($value, Constraint $constraint)
    {
        foreach ($value as $ticket) {
            $settings = $this->orderManager->getTicketSettings($ticket);
            if ($settings['countInValidOrder']) {
                return ;
            }
        }
        $this->context->buildViolation($constraint->message)
            ->addViolation();

    }

}