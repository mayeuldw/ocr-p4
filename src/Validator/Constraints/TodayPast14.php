<?php
/**
 * Created by PhpStorm.
 * User: mayeu
 * Date: 20/04/2018
 * Time: 21:59
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TodayPast14 extends Constraint
{
    public $message = 'Le billet journée entière n\'est pas disponible pour cette date';
}
