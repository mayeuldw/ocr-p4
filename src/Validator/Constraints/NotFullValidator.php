<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 15/03/2018
 * Time: 13:23
 */

namespace App\Validator\Constraints;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NotFullValidator extends ConstraintValidator
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function validate($value, Constraint $constraint)
    {
        $available = Order::TICKET_MAX_PER_DAY - $this->orderRepository->getDaySoldTicket($value->getDate());
        if ($available < $value->getNumber()) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value->getDate()->format('d/m/Y'))
                ->addViolation();
        }

    }




}