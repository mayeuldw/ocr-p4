<?php
/**
 * Created by PhpStorm.
 * User: mayeu
 * Date: 20/04/2018
 * Time: 21:59
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TodayPast14Validator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $now = new \DateTime();
        $today = $value->format('Y-m-d') === $now->format('Y-m-d');
        $hour = $now->format('H');
        if ($today && $hour >= 14) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }

    }
}
