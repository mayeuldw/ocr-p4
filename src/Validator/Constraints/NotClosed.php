<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 15/03/2018
 * Time: 13:21
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotClosed extends Constraint
{
    public $message = 'La date choisie n\'est pas disponible pour la réservation';
}