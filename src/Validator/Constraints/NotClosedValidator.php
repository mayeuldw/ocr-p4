<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 15/03/2018
 * Time: 13:23
 */

namespace App\Validator\Constraints;

use App\Manager\BankHolidaysManager;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NotClosedValidator extends ConstraintValidator
{
    protected $bankHolidaysManager;

    public function __construct(BankHolidaysManager $bankHolidaysManager)
    {
        $this->bankHolidaysManager = $bankHolidaysManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->bankHolidaysManager->isClosed($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value->format('d/m/Y'))
                ->addViolation();
        }
    }

}