<?php
/**
 * Created by PhpStorm.
 * User: mayeu
 * Date: 20/04/2018
 * Time: 22:00
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TodayPast14FullDayValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $now = new \DateTime();
        $today = $value->getDate()->format('Y-m-d') === $now->format('Y-m-d');
        $hour = $now->format('H');
        if ($today && $hour >= 14 && $value->getFullDay()) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }

    }
}
