<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function save(Order $order)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($order);
        $entityManager->flush();
    }

    public function getDaySoldTicket(\DateTime $date) : int
    {
        $qb = $this->createQueryBuilder('o');
        $result = $qb->select('count(t)')
            ->innerJoin('o.tickets', 't')
            ->where('o.date = :date')->setParameter('date',  $date->format('Y-m-d'))
            ->getQuery();

        return $result->getSingleScalarResult();
    }

    public function getMonthSoldTicket(\DateTime $date) : array
    {
        $qb = $this->createQueryBuilder('o');
        $result = $qb->select('count(t) as tot, o.date')
            ->innerJoin('o.tickets', 't')
            ->where('o.date LIKE :date')->setParameter('date',  $date->format('Y-m-%'))
            ->groupBy('o.date')
            ->getQuery()
            ->getResult();
        $resultByDate = [];
        foreach ($result as $row) {
            $resultByDate[$row['date']->format('Y-m-d')] = $row['tot'];
        }
        return $resultByDate;
    }



}
