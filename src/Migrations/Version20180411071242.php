<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180411071242 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD first_name VARCHAR(255) NOT NULL, ADD order_address VARCHAR(255) NOT NULL, CHANGE order_adress name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE tickets CHANGE nationality country VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD order_adress VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP name, DROP first_name, DROP order_address');
        $this->addSql('ALTER TABLE tickets CHANGE country nationality VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
