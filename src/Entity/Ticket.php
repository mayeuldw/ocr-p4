<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="tickets")
 */
class Ticket
{
    const DAY_SETTINGS =
    [
        [
            'title' => 'Journée entière',
            'price' => 1,
            'value' => true
        ],
        [
            'title' => 'Demi-journée',
            'price' => 0.6,
            'value' => false
        ],
    ];

    const SETTINGS =
    [
        [
            'display' => 4,
            'nom' => 'Enfant en bas age',
            'comment'=> '',
            'reduceTicket' => null,
            'price' => 0,
            'ageCondMax' => 4,
            'countInValidOrder' => false
        ],
        [
            'display' => 1,
            'nom' => 'Enfant',
            'comment'=> '',
            'reduceTicket' => null,
            'price' => 8,
            'ageCondMax' => 12,
            'countInValidOrder' => false
        ],
        [
            'display' => 0,
            'nom' => 'Adulte',
            'comment'=> '',
            'reduceTicket' => false,
            'price' => 16,
            'ageCondMax' => 60,
            'countInValidOrder' => true
        ],
        [
            'display' => 2,
            'nom' => 'Senior',
            'comment'=> '',
            'reduceTicket' => false,
            'price' => 12,
            'countInValidOrder' => true
        ],
        [
            'display' => 3,
            'nom' => 'Tarif réduit',
            'comment'=> 'Il sera nécessaire de présenter votre(/vos) carte(s) d’étudiant, militaire ou équivalent lors de l’entrée',
            'reduceTicket' => true,
            'price' => 10,
            'countInValidOrder' => true
        ]
    ];
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="tickets")
     */
    protected $order;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Le nom ne doit pas etre vide", groups={"visitor"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Le prénom ne doit pas etre vide", groups={"visitor"})
     */
    protected $firstName;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\Country(message="Vous devez choisir un pays dans la liste", groups={"visitor"})
     */
    protected $country  = 'FR';

    /**
     * @ORM\Column(type="date")
     * @Assert\LessThanOrEqual("today", message="La date de naissance doit etre passée", groups={"visitor"})
     */
    protected $birthDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $reducedPrice = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $ticketHash;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    protected $price;




    /*****
     * 
     * GETTER AND SETTERS 
     ****/
    public function getId() : int
    {
        return $this->id;
    }

    public function getOrder() : Order
    {
        return $this->order;
    }
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    public function getName() : ? string
    {
        return $this->name;
    }
    public function setName(string $name)
    {
        $this->name = htmlspecialchars($name);
    }

    public function getFirstName() : ? string
    {
        return $this->firstName;
    }
    public function setFirstName(string $firstName)
    {
        $this->firstName = htmlspecialchars($firstName);
    }

    public function getCountry() : string
    {
        return $this->country;
    }
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    public function getBirthDate() : ? \DateTime
    {
        return $this->birthDate;
    }
    public function setBirthDate(\DateTime $birthDate)
    {
        $this->birthDate =$birthDate;
    }

    public function getReducedPrice() : bool
    {
        return $this->reducedPrice;
    }
    public function setReducedPrice(bool $reducedPrice)
    {
        $this->reducedPrice = $reducedPrice;
    }

    public function getTicketHash() : string
    {
        return $this->ticketHash;
    }
    public function setTicketHash(string $ticketHash)
    {
        $this->ticketHash = htmlspecialchars($ticketHash);
    }

    public function getType() : ? string
    {
        return $this->type;
    }
    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getPrice() : ? int
    {
        return $this->price;
    }
    public function setPrice(int $price)
    {
        $this->price = $price;
    }


    public function getAgeAtVisiteDay()
    {
        return $this->getOrder()->getDate()->diff($this->getBirthDate())->format('%y');
    }
}
