<?php
namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 * @ORM\Table(name="orders")
 * @AppAssert\NotFull(groups={"dateAndNumber"})
 * @AppAssert\TodayPast14FullDay(groups={"todayPastHalfJourney"})
 */
class Order
{
    const TICKET_MAX_PER_DAY    = 1000;

    const CLOSED_DAYS = [
        ['w'=> '0'],
        ['w'=> '2']
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="date")
     * @Assert\GreaterThanOrEqual("today", message="La date ne doit pas etre passée", groups={"dateAndNumber"})
     * @AppAssert\NotClosed(groups={"dateAndNumber"})
     * @AppAssert\TodayPast14(groups={"todayPast14"})
     */
    protected $date;

    /**
     * Not Save in BDD
     *
     * @Assert\GreaterThan(value = 0, message="Le nombre de visiteur doit etre supérieur à zero", groups={"dateAndNumber"})
     */
    protected $number;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="L'email de ne doit pas etre vide", groups={"billing"})
     * @Assert\Email(message="L'email ne doit etre valide", groups={"billing"})
     */
    protected $email;

    /**
     * Not Save in BDD
     *
     * @Assert\EqualTo(propertyPath="email", groups={"billing"})
     */
    protected $email2;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Le nom de ne doit pas etre vide", groups={"billing"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Le prénom de ne doit pas etre vide", groups={"billing"})
     */
    protected $firstName;

    /**
     * Not Save in BDD
     *
     * @Assert\NotBlank(message="L'adresse ne doit pas etre vide", groups={"billing"})
     */
    protected $formAddress1;

    /**
     * Not Save in BDD
     */
    protected $formAddress2;

    /**
     * Not Save in BDD
     *
     * @Assert\NotBlank(message="Le code postal ne doit pas etre vide", groups={"billing"})
     */
    protected $formCP;

    /**
     * Not Save in BDD
     *
     * @Assert\NotBlank(message="La ville ne doit pas etre vide", groups={"billing"})
     */
    protected $formCity;

    /**
     * Not Save in BDD
     *
     * @Assert\NotBlank(message="Le pays ne doit pas etre vide", groups={"billing"})
     */
    protected $formCountry = 'FR';

    /**
     * @ORM\Column(type="string")
     */
    protected $orderAddress ='';

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="order", cascade={"persist"})
     * @Assert\Valid(groups={"visitor"})
     * @AppAssert\OneAdultTicket(groups={"visitor"})
     * @Assert\NotBlank(groups={"initVisitor"})
     */
    protected $tickets;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $fullDay = true;

    /**
     * @ORM\Column(type="string")
     */
    protected $billingId;





    /*****
     * 
     * Construct ***
     *
     * @param \DateTime $date
     * @param int       $number
     */
    public function __construct(DateTime $date, int $number)
    {
        $this->date = $date;
        $this->number = $number;
        $this->tickets = new ArrayCollection();
    }


    /*****
     * 
     * Tickets Management 
     **/
    public function addTicket(Ticket $ticket)
    {
        $this->tickets->add($ticket);
    }

    public function removeAllTickets()
    {
        $this->tickets = new ArrayCollection();
    }
    public function removeTicket(Ticket $ticket)
    {
        $this->tickets->removeElement($ticket);
    }

    public function initializeTickets()
    {
        while($this->number !== count($this->tickets)) {
            if($this->number > count($this->tickets)) {
                $this->addTicket(new Ticket($this));
            }
            else {
                $this->removeTicket($this->tickets->last());
            }
        }
    }


    /*****
     * 
     * GETTER AND SETTERS 
     ****/
    public function getId() : int
    {
        return $this->id;
    }

    public function getDate() : \DateTime
    {
        return $this->date;
    }
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getNumber() : int
    {
        return $this->number ?? count($this->tickets);
    }
    public function setNumber(int $number)
    {
        $this->number = $number;
    }

    public function getEmail() : ? string
    {
        return $this->email;
    }
    public function setEmail(string $email)
    {
        $this->email = htmlspecialchars($email);
    }

    public function getEmail2() : ? string
    {
        return $this->email2;
    }
    public function setEmail2(string $email2)
    {
        $this->email2 = htmlspecialchars($email2);
    }

    public function getName() : ? string
    {
        return $this->name;
    }
    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getFirstName() : ? string
    {
        return $this->firstName;
    }
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFormAddress1() : ? string
    {
        return $this->formAddress1;
    }
    public function setFormAddress1(string $formAddress1)
    {
        $this->formAddress1 = $formAddress1;
    }

    public function getFormAddress2() : ? string
    {
        return $this->formAddress2;
    }
    public function setFormAddress2(string $formAddress2 = null)
    {
        $this->formAddress2 = $formAddress2;
    }

    public function getFormCP() : ? string
    {
        return $this->formCP;
    }
    public function setFormCP(string $formCP)
    {
        $this->formCP = $formCP;
    }

    public function getFormCity() : ? string
    {
        return $this->formCity;
    }
    public function setFormCity(string $formCity)
    {
        $this->formCity = $formCity;
    }

    public function getFormCountry() : ? string
    {
        return $this->formCountry;
    }
    public function setFormCountry(string $formCountry)
    {
        $this->formCountry = $formCountry;
    }

    public function getOrderAddress() : string
    {
        return $this->orderAddress;
    }
    public function setOrderAddress(string $orderAddress)
    {
        $this->orderAddress = htmlspecialchars($orderAddress);
    }

    public function getTicket() : Collection
    {
        return $this->tickets;
    }

    public function getFullDay() : bool
    {
        return $this->fullDay;
    }
    public function setFullDay(bool $fullDay)
    {
        $this->fullDay = $fullDay;
    }

    public function getBillingId() : ? string
    {
        return $this->billingId;
    }

    public function setBillingId(string $billingId)
    {
        $this->billingId = $billingId;
    }
}
