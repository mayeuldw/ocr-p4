<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 15/03/2018
 * Time: 14:24
 */

namespace App\Manager;

use App\Entity\Order;

class BankHolidaysManager
{
    private $bankHolidays = [];

    public function isClosed(\DateTime $date) : bool
    {
        $bankHolidays = $this->get(intval($date->format('Y')));
        if (key_exists($date->format('m-d'), $bankHolidays)) {
            return true;
        }


        foreach (Order::CLOSED_DAYS as $day) {
            if ($date->format(implode('_', array_keys($day))) === implode('_', $day)) {
                return true;
            }
        }
        return false;
    }

    private function get(int $year) : array
    {
        if (!key_exists($year, $this->bankHolidays)) {
            $this->bankHolidays[$year] = $this->getBankHolidays($year);
        }
        return $this->bankHolidays[$year];
    }

    public function getBankHolidays(int $year)
    {
        $easterTime = easter_date($year);
        return [
            '01-01' => 'Jour de l\'an',
            date('m-d', strtotime('+ 1 day', $easterTime)) => 'Lundi de Paques',
            date('m-d', strtotime('+ 39 day', $easterTime)) => 'Ascension',
            date('m-d', strtotime('+ 50 day', $easterTime)) => 'Pentecôte',
            '05-01' => 'Fete du Travail',
            '05-08' => '8 Mai 1945',
            '07-14' => 'Fete Nationale',
            '08-15' => 'Assomption',
            '11-01' => 'La Toussaint',
            '11-11' => 'Armistice',
            '12-25' => 'Noel',

        ];
    }
}
