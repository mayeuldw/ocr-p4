<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 11/04/2018
 * Time: 11:59
 */

namespace App\Manager;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PriceManager
{
    protected $total = 0;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $sessionToken = $this->session->get('Hash', base64_encode(random_bytes(32)));
        $this->session->set('Hash', $sessionToken);
    }

    public function resetTotal()
    {
        $this->total = 0;
    }

    public function addPriceToTotal($ticketPrice, $dayPercent, $quantity)
    {
        $price = $quantity * $this->getPrice($ticketPrice, $dayPercent);
        $this->total += $price;
        return $price;

    }

    public function getPrice($ticketPrice, $dayPercent)
    {
        return number_format($dayPercent * $ticketPrice, 2, '.', '');
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getStripeTotal()
    {
        return 100*$this->total;
    }

    public function getDaySettingsFromToken(array $daySettings, string $token) : ? array
    {
        foreach ($daySettings as $settings) {
            if($this->getToken($settings) === $token) {
                return $settings;
            }
        }
        return null;
    }

    public function getToken($ticketType)
    {
        return sha1($this->session->get('Hash').$ticketType['price'].$ticketType['title']);
    }
}
