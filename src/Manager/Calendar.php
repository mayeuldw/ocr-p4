<?php
namespace App\Manager;

use App\Entity\Order;
use App\Repository\OrderRepository;
use DateTime;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class Calendar
{
    protected $currentDate;
    protected $startDate;

    protected $currentMonth;
    protected $previousMonth;
    protected $nextMonth;

    private  $bankHolidaysManager;
    /**
     * @var OrderManager
     */
    private $orderManager;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(BankHolidaysManager $bankHolidaysManager, OrderRepository $orderRepository, OrderManager $orderManager, SessionInterface $session) 
    {
        $this->bankHolidaysManager = $bankHolidaysManager;
        $this->orderManager = $orderManager;
        $this->orderRepository = $orderRepository;
        $this->session = $session;

        $this->currentMonth = new DateTime('midnight first day of this month');
        $this->setStartDate();
    }

    function setStartDate(DateTime $startDate = null)
    {
        $this->startDate = $startDate ?? $this->session->get('calendarDefaultDateTime', $this->orderManager->getDefaultDate());
        $this->session->set('calendarDefaultDateTime', $this->startDate);
        $this->startDate->modify('first day of this month');

        $this->previousMonth = (clone $this->startDate)->modify('-1 MONTH');
        $this->nextMonth     = (clone $this->startDate)->modify('+1 MONTH');
    }


    function formatMonth(Datetime $datetime) : string
    {
        $date = $datetime->format('Y-m-d');
        $month = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        return $month[substr($date, 5, 2) -1].' '.substr($date, 0, 4);
    }


    public function previousMonth()
    {
        if ($this->previousMonth >= $this->currentMonth) {
            $this->setStartDate($this->previousMonth);
        }
    }

    public function nextMonth()
    {
        $this->setStartDate($this->nextMonth);
    }

    public function getStartDate() : string
    {
        return $this->startDate;
    }


    public function hasPreviousMonth() : bool
    {
        return $this->previousMonth < $this->currentMonth;
    }

    public function previousMonthTitle() : string
    {
        return $this->formatMonth($this->previousMonth);
    }

    public function currentMonthTitle() : string
    {
        return $this->formatMonth($this->startDate);
    }

    public function nextMonthTitle() : string
    {
        return $this->formatMonth($this->nextMonth);
    }

    public function days() : array
    {
        $days = [];
        $today = new DateTime('today');
        $dayInMonth = $this->startDate->format('t')+0;
        $sold = $this->orderRepository->getMonthSoldTicket($this->startDate);
        $full = Order::TICKET_MAX_PER_DAY;
        for ($i = 1; $i <= $dayInMonth; $i++) {
            $date = $this->startDate->format('Y-m-').($i<10 ? '0'.$i:$i);
            $datetime = new DateTime($date);
            $days[] = [
                'd'=>$i,
                'date' => $date,
                'w'=> $datetime->format('w') +0,
                'q'=> $full - ($sold[$date] ?? 0),
                'isClose' => $datetime < $today || $this->bankHolidaysManager->isClosed($datetime)
            ];
        }
        return $days;
    }
}
