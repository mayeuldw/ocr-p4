<?php
/**
 * Created by PhpStorm.
 * User: mayeu
 * Date: 02/05/2018
 * Time: 09:40
 */

namespace App\Manager;


use App\Entity\Order;
use Swift_Image;
use Swift_Mailer;
use Swift_Message;
use Twig_Environment;

class MailerManager
{
    private $confirmSubject = 'Votre visite au louvre';

    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Twig_Environment
     */
    private $twigEnvironment;
    /**
     * @var string
     */
    private $mailerFrom;
    /**
     * @var string
     */
    private $publicDir;

    /**
     * MailerManager constructor.
     *
     * @param string           $mailerFrom
     * @param string           $mailerName
     * @param string           $rootDir
     * @param Twig_Environment $twigEnvironment
     * @param Swift_Mailer     $mailer
     */
    function __construct(string $mailerFrom, string $mailerName, string $rootDir, Twig_Environment $twigEnvironment, Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->twigEnvironment = $twigEnvironment;
        $this->mailerFrom  = [$mailerFrom => $mailerName];
        $this->publicDir = $rootDir.'/../public';
    }

    private function sendMail(string $to, string $subject, string $layout, array $extract = []) : bool
    {
        $message = (new Swift_Message($subject))
            ->setFrom($this->mailerFrom)
            ->setTo($to);
        $extract['message'] = $message;

        return $this->mailer->send($message->setBody($this->twigEnvironment->render($layout, $extract), 'text/html'));
    }

    public function sendConfirmOrderMail(Order $order)
    {
        return $this->sendMail(
            $order->getEmail(), $this->confirmSubject, 'mail/mail.twig', [
            'order' => $order,
            'logo' => Swift_Image::fromPath($this->publicDir.'/assets/louvre.png')]
        );
    }


}
