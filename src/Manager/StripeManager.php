<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 16/04/2018
 * Time: 09:25
 */

namespace App\Manager;



use Exception;
use Stripe\Charge;
use Stripe\Stripe;

class StripeManager
{
    public function __construct($stripePrivateKey)
    {
        Stripe::setApiKey($stripePrivateKey);

    }

    public function process(string $stripeToken, int $price) : bool
    {
        if ($stripeToken === null) { return false;
        }
        try
        {
            Charge::create(
                [
                'amount' => $price,
                'currency' => 'eur',
                'description' => 'Visite au louvre',
                'source' => $stripeToken,
                ]
            );
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }
}
