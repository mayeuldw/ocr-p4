<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 26/03/2018
 * Time: 11:37
 */

namespace App\Manager;


use App\Entity\Order;
use App\Entity\Ticket;
use App\Exception\InvalidOrderException;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderManager
{
    /**
     * @var SessionInterface
     */
    private $session;

    const DATE_AND_NUMBER = 1;
    const VISITOR = 2;
    const BILLING = 3;
    const SUM_UP = 4;
    /**
     * @var OrderRepository
     */
    private $bankHolidaysManager;
    /**
     * @var PriceManager
     */
    private $priceManager;
    /**
     * @var StripeManager
     */
    private $stripeManager;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var MailerManager
     */
    private $mailer;


    public function __construct(
        SessionInterface $session,
        BankHolidaysManager $bankHolidaysManager,
        OrderRepository $orderRepository,
        PriceManager $priceManager,
        StripeManager $stripeManager,
        ValidatorInterface $validator,
        MailerManager $mailer
    ) {
        $this->session = $session;
        $this->bankHolidaysManager = $bankHolidaysManager;
        $this->orderRepository = $orderRepository;
        $this->priceManager = $priceManager;
        $this->stripeManager = $stripeManager;
        $this->validator = $validator;
        $this->mailer = $mailer;
    }

    public function getDefaultDate()
    {
        $date = new \DateTime('tomorrow');
        while ($this->bankHolidaysManager->isClosed($date)) {
            $date->modify('+1 Day');
        }
        return $date;
    }

    public function processTicketType(Order $order)
    {
        $ticketsSettings = Ticket::SETTINGS;
        usort(
            $ticketsSettings, function ($first,$second) {
                return $first['display'] > $second['display'];
            }
        );
        $recap = [];
        foreach ($ticketsSettings as $v) {
            $recap[$v['nom']] = $v;
        }

        $tickets = $order->getTicket();
        foreach ($tickets as $ticket) {
            $settings = $this->getTicketSettings($ticket);
            $ticket->setType($settings['nom']);
            $recap[$settings['nom']]['count'] = 1 + ($recap[$settings['nom']]['count'] ?? 0);
        }
        return $recap;
    }

    public function getTicketSettings(Ticket $ticket) : array
    {
        $age = $ticket->getAgeAtVisiteDay();
        $settings = Ticket::SETTINGS;
        foreach ($settings as $settings) {
            if ($settings['reduceTicket'] !== null && $settings['reduceTicket'] !== $ticket->getReducedPrice()) {
                continue;
            }
            if (key_exists('ageCondMax', $settings) && $settings['ageCondMax'] < $age) {
                continue;
            }
            if ($settings['reduceTicket'] === null) {
                $ticket->setReducedPrice(false);
            }
            return $settings;
        }
        // If configuration does not match : get the last one
        return end($settings);
    }

    public function getAvailableDaySettings(Order $order)
    {
        $filter = count($this->validator->validate($order, null, ['todayPast14']))> 0;
        $result = [];
        foreach (Ticket::DAY_SETTINGS as $settings) {
            if (!$filter || !$settings['value']) {
                $result[] = $settings;
            }
        }
        return $result;
    }

    public function processStripePayment(Order $order, string $token, string $stripeToken, string $price) : bool
    {
        $daySettings = $this->priceManager->getDaySettingsFromToken($this->getAvailableDaySettings($order), $token);
        if ($daySettings === null) {
            return $this->cancelStripeOrder();
        }

        $priceByType = $this->ticketPriceByType();

        $this->priceManager->resetTotal();
        $order->setFullDay($daySettings['value']);
        foreach ($order->getTicket() as $ticket) {
            if (!key_exists($ticket->getType(), $priceByType)) {
                return $this->cancelStripeOrder();
            }
            $basePrice = $priceByType[$ticket->getType()];
            $percentPrice = $daySettings['price'];
            $ticket->setPrice($this->priceManager->addPriceToTotal($basePrice, $percentPrice, 1));
        }

        $orderTotalPrice = $this->priceManager->getStripeTotal();
        if ($orderTotalPrice != intval($price)) {
            return $this->cancelStripeOrder();
        }

        $order->setBillingId($stripeToken);
        if ($this->validateForPayment($order)) {

            if ($this->stripeManager->process($stripeToken, $orderTotalPrice)) {
                $this->orderRepository->save($order);
                $this->mailer->sendConfirmOrderMail($order);
                $this->session->set('OrderId', $order->getId());
                $this->session->set('Order', null);
                return true;
            }
        }
        return $this->cancelStripeOrder();
    }

    private function ticketPriceByType() : array
    {
        $result = [];
        foreach (Ticket::SETTINGS as $settings) {
            $result[$settings['nom']] = $settings['price'];
        }
        return $result;
    }

    private function cancelStripeOrder() : bool
    {
        return false;
    }

    public function getPaidOrder() : ? Order
    {
        $orderId = $this->session->get('OrderId');
        if ($orderId === null) {
            throw new InvalidOrderException();
        }
        $order = $this->orderRepository->find($orderId);
        if ($order === null) {
            throw new InvalidOrderException();
        }
        return $order;
    }

    public function getOrder(string $step) : Order
    {
        $order = $this->session->get('Order');
        if ($order === null) {
            if ($step != self::DATE_AND_NUMBER) {
                throw new InvalidOrderException();
            }
            $order = new Order($this->getDefaultDate(), 2);
            $this->session->set('Order', $order);
        }

        switch($step)
        {
        case self::DATE_AND_NUMBER:     
            return $this->validateFor($order, []);
        case self::VISITOR:             
            return $this->validateFor($order, ['dateAndNumber', 'initVisitor']);
        case self::BILLING:             
            return $this->validateFor($order, ['dateAndNumber', 'visitor']);
        case self::SUM_UP:              
            return $this->validateFor($order, ['dateAndNumber', 'visitor', 'billing']);
        }
        throw new InvalidOrderException();
    }

    private function validateFor(Order $order, array $validationGroup) : Order
    {
        $errors = $this->validator->validate($order, null, $validationGroup);
        if (count($errors) > 0) {
            throw new InvalidOrderException();
        }
        return $order;
    }

    public function validateForPayment(Order $order)
    {
        $violations = $this->validator->validate(
            $order, null, [
                'dateAndNumber',
                'visitor',
                'billing',
                'todayPastHalfJourney']
        );
        return count($violations) === 0;
    }

}
