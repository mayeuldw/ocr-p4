<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 18/05/2018
 * Time: 11:49
 */

namespace App\Tests\Manager;
use App\Manager\BankHolidaysManager;
use PHPUnit\Framework\TestCase;

class BankHolidaysManagerTest extends TestCase
{
    public function testBankHolidays2018()
    {
        $bankHolidays = new BankHolidaysManager();
        $result = $bankHolidays->getBankHolidays(2018);
        $this->assertEquals([
            '01-01' => 'Jour de l\'an',
            '04-02' => 'Lundi de Paques',
            '05-10' => 'Ascension',
            '05-21' => 'Pentecôte',
            '05-01' => 'Fete du Travail',
            '05-08' => '8 Mai 1945',
            '07-14' => 'Fete Nationale',
            '08-15' => 'Assomption',
            '11-01' => 'La Toussaint',
            '11-11' => 'Armistice',
            '12-25' => 'Noel'
        ], $result);
    }
}