<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 18/05/2018
 * Time: 12:02
 */

namespace App\Tests\Manager;

use App\Manager\PriceManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PriceManagerTest extends TestCase
{
    /** @var PriceManager */
    protected $priceManager;

    public function setUp()
    {
        $sessionInterface = $this
            ->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $sessionInterface
            ->method('get')
            ->willReturn('tok');

        $this->priceManager = new PriceManager($sessionInterface);
    }

    public function tearDown()
    {
        $this->priceManager = null;
    }

    public function testProcessReset()
    {
        $this->priceManager->resetTotal();
        $this->assertEquals(0, $this->priceManager->getTotal());
        $this->assertEquals(0, $this->priceManager->getStripeTotal());
    }

    /**
     * @dataProvider providerProcessAdd
     * @param $ticketPrice
     * @param $dayPercent
     * @param $quantity
     * @param $expected
     */
    public function testProcessAdd($ticketPrice, $dayPercent, $quantity, $expected)
    {
        $this->priceManager->resetTotal();
        $this->priceManager->addPriceToTotal($ticketPrice, $dayPercent, $quantity);
        $this->assertEquals($expected, $this->priceManager->getTotal());
        $this->assertEquals(($expected * 100), $this->priceManager->getStripeTotal());
    }


    public function providerProcessAdd(){
        return [
            [10.958, 1 ,1 , 10.96],
            [10, 0.45 ,1 , 4.50],
        ];
    }



}