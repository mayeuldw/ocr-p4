<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 18/05/2018
 * Time: 11:49
 */

namespace App\Tests\Manager;
use App\Entity\Order;
use App\Entity\Ticket;
use App\Manager\BankHolidaysManager;
use App\Manager\MailerManager;
use App\Manager\OrderManager;
use App\Manager\PriceManager;
use App\Manager\StripeManager;
use App\Repository\OrderRepository;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderManagerTest extends TestCase
{
    /** @var OrderManager */
    private $orderManager;

    public function setUp()
    {
        $sessionInterface = $this
            ->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $sessionInterface
            ->method('get')
            ->willReturn('tok');

        $bankHolidaysManager = $this
            ->getMockBuilder(BankHolidaysManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRepository = $this
            ->getMockBuilder(OrderRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $priceManager = $this
            ->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stripeManager = $this
            ->getMockBuilder(StripeManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator = $this
            ->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mailer = $this
            ->getMockBuilder(MailerManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderManager = new OrderManager($sessionInterface, $bankHolidaysManager, $orderRepository, $priceManager, $stripeManager, $validator, $mailer);
    }

    public function tearDown()
    {
        $this->orderManager = null;
    }

    /**
     * @dataProvider providerGetTicketSettings
     * @param $visitDate
     * @param $ticketBirthDate
     * @param $reducedPrice
     * @param $expected
     */
    function testGetTicketSettings($visitDate, $ticketBirthDate, $reducedPrice,  $expected)
    {
        $order = new Order(new DateTime($visitDate), 1);
        $ticket = new Ticket($order);
        $ticket->setBirthDate(new DateTime($ticketBirthDate));
        $ticket->setReducedPrice($reducedPrice);
        $order->addTicket($ticket);

        $ticketType = $this->orderManager->getTicketSettings($ticket)['nom'];
        $this->assertEquals($expected, $ticketType);
    }

    public function providerGetTicketSettings(){
        return [
            ['2018-01-01', '2000-01-01', false, 'Adulte'],
            ['2018-01-01', '2010-01-01', false, 'Enfant'],
            ['2018-01-01', '2017-01-01', false, 'Enfant en bas age'],
            ['2018-01-01', '1910-01-01', false, 'Senior'],

            ['2018-01-01', '2000-01-01', true, 'Tarif réduit'],
            ['2018-01-01', '2010-01-01', true, 'Enfant'],
            ['2018-01-01', '2017-01-01', true, 'Enfant en bas age'],
            ['2018-01-01', '1910-01-01', true, 'Tarif réduit'],
        ];
    }


    /**
     * @dataProvider providerProcessTicketType
     * @param $visitDate
     * @param $tickets
     * @param $expected
     */
    function testProcessTicketType($visitDate, $tickets, $expected)
    {
        $order = new Order(new DateTime($visitDate), 1);
        foreach($tickets as $t)
        {
            $ticket = new Ticket($order);
            $ticket->setBirthDate(new DateTime($t[0]));
            $ticket->setReducedPrice($t[1]);
            $order->addTicket($ticket);
        }

        $recap = $this->orderManager->processTicketType($order);
        foreach($recap as $key=>$type)
        {
            if(key_exists($key, $expected))
            {
                $this->assertSame($type['count'], $expected[$key]);
            }
            else
            {
                $this->assertTrue(!isset($type['count']));
            }
        }
    }


    function providerProcessTicketType()
    {
        return [
            [
                '2018-01-01', [
                    ['2000-01-01', false],
                    ['2010-01-01', false],
                    ['2017-01-01', false],
                    ['1910-01-01', false],
                ], [
                    'Adulte'            => 1,
                    'Enfant'            => 1,
                    'Senior'            => 1,
                    'Enfant en bas age' => 1
                ]
            ],
            [
                '2018-01-01', [
                    ['2000-01-01', true],
                    ['2010-01-01', true],
                    ['2017-01-01', true],
                    ['1910-01-01', true],
                ], [
                    'Tarif réduit'      => 2,
                    'Enfant'            => 1,
                    'Enfant en bas age' => 1
                ]
            ]
        ];
    }
}