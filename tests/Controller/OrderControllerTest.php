<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 21/05/2018
 * Time: 09:20
 */

namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderControllerTest extends WebTestCase
{

    public function testHomepage()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider providerRoutesNotHomepage
     * @param $route
     */
    public function testDirectNotHomepage($route)
    {
        $client = static::createClient();
        $client->request('GET', $route);
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->isRedirect('/'));
    }

    public function providerRoutesNotHomepage()
    {
        return [['/visitor'], ['/billing'], ['/summary'], ['/success']];
    }

    public function testWorkFlow()
    {

        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $form = $crawler->selectButton('Valider et passer à l\'étape suivante')->form();
        $form['date_and_number[number]'] = 2;

        $client->submit($form);
        $crawler = $client->followRedirect();

        $form = $crawler->selectButton('Valider et passer à l\'étape suivante')->form();
        $form['visitor[ticket][0][name]'] = 'Doe';
        $form['visitor[ticket][0][firstName]'] = 'John';
        $form['visitor[ticket][0][birthDate]'] = '2000-01-01';
        $form['visitor[ticket][0][country]'] = 'US';

        $form['visitor[ticket][1][name]'] = 'Martin';
        $form['visitor[ticket][1][firstName]'] = 'Alice';
        $form['visitor[ticket][1][birthDate]'] = '2010-01-01';
        $form['visitor[ticket][1][country]'] = 'FR';


        $client->submit($form);
        $crawler = $client->followRedirect();

        $form = $crawler->selectButton('Valider et passer à l\'étape suivante')->form();
        $form['billing[email]'] = 'mon.adresse@email.fr';
        $form['billing[email2]'] = 'mon.adresse@email.fr';
        $form['billing[name]'] = 'Doe';
        $form['billing[firstName]'] = 'John';

        $form['billing[formAddress1]'] = '1 rue du bon endroit';
        $form['billing[formAddress2]'] = 'Bat B';
        $form['billing[formCP]'] = '01200';
        $form['billing[formCity]'] = 'Bellegarde-Sur-Vaslerine';
        $form['billing[formCountry]'] = 'FR';

        $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect('/summary'));

    }
}