<?php
/**
 * Created by PhpStorm.
 * User: Mayeul
 * Date: 21/05/2018
 * Time: 09:49
 */


require __DIR__.'/vendor/autoload.php';


if (isset($_ENV['BOOTSTRAP_CLEAR_CACHE_ENV'])) {
    passthru(sprintf(
        'php "bin/console" cache:clear --env=%s --no-warmup',
        $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']
    ));
}
//require __DIR__ . '/bootstrap.php.cache';
//drop/create database
passthru(sprintf('php "bin/console" doctrine:database:drop --env=test --force')); // force used to avoid interaction
//create database
passthru(sprintf('php "bin/console" doctrine:database:create --env=test'));
//init database
passthru(sprintf('php "bin/console" doctrine:schema:create --env=test'));
// add custom initializations here, such as loading trigger, stored procedure or custom SQL
